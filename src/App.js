import React from 'react';
import './App.css';

// Components
import Widget from './components/widget/Widget';

function App() {
  return (
    <div className="App">
      <Widget />
    </div>
  );
}

export default App;
