import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Components
import WidgetView from './WidgetView';

// Constants
import { COMPLETE_TASK, UNCOMPLETE_TASK } from './constants/constantsWidget';

const propTypes = {
  getTasks: PropTypes.func.isRequired,
  changeTaskStatus: PropTypes.func.isRequired,
  openTasks: PropTypes.array,
  doneTasks: PropTypes.array,
};

const defaultProps = {
  openTasks: [],
  doneTasks: [],
};

class WidgetController extends Component {
  state = {};

  componentDidMount = () => {
    const { getTasks } = this.props;

    getTasks();
  };

  handleChangeCheckbox = (evt, id) => {
    const { changeTaskStatus } = this.props;

    changeTaskStatus({
      id,
      status: evt.target.checked ? COMPLETE_TASK : UNCOMPLETE_TASK,
    });
  };

  render() {
    return (
      <WidgetView
        {...this.props}
        handleChangeCheckbox={this.handleChangeCheckbox}
      />
    );
  }
}

WidgetController.propTypes = propTypes;
WidgetController.defaultProps = defaultProps;

export default WidgetController;
