import { connect } from 'react-redux';

// Actions
import { getTasks, changeTaskStatus } from './actions/actionsWidget';

// Components
import WidgetController from './WidgetController';

const mapStateToProps = ({ reducersWidget }) => {
  const { openTasks, doneTasks } = reducersWidget;

  return {
    openTasks,
    doneTasks,
  };
};

const mapDispatchToProps = { getTasks, changeTaskStatus };

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(WidgetController);
