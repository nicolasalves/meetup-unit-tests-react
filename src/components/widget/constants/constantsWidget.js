// Action Types
export const GET_TASKS = 'GET_TASKS';
export const CHANGE_TASK_STATUS = 'CHANGE_TASK_STATUS';

// Constants
export const COMPLETE_TASK = 'COMPLETE_TASK';
export const UNCOMPLETE_TASK = 'UNCOMPLETE_TASK';

export const responseMock = {
  openTasks: [
    { label: 'Ir ao shopping', id: 0 },
    { label: 'Terminar o dever de casa', id: 1 },
    { label: 'Lavar louça', id: 2 },
    { label: 'Ir ao trabalho', id: 3 },
  ],
  doneTasks: [{ label: 'Fazer lanche', id: 4 }],
};
