import React from 'react';
import { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer';

// Components
import WidgetController from '../WidgetController';
import WidgetView from '../WidgetView';

// Reducers
import reducersWidget from '../reducers/reducersWidget';

// Constants
import * as constantsWidget from '../constants/constantsWidget';

const defaultProps = {
  getTasks: jest.fn(),
  changeTaskStatus: jest.fn(),
  openTasks: [],
  doneTasks: [],
};

// Devemos fazer testes mais legíveis
describe('O Widget deve', () => {
  // View tests
  it('renderizar corretamente o componente', () => {
    const WidgetComponent = renderer
      .create(<WidgetView {...defaultProps} />)
      .toJSON();

    expect(WidgetComponent).toMatchSnapshot();
  });

  // Props tests
  it('renderizar com props custom', () => {
    const WidgetComponent = shallow(
      <WidgetView
        {...defaultProps}
        openTasks={[
          { label: 'Open Task 0', id: 0 },
          { label: 'Open Task 1', id: 1 },
        ]}
        doneTasks={[
          { label: 'Open Task 2', id: 2 },
          { label: 'Open Task 3', id: 3 },
        ]}
      />,
    );

    expect(
      WidgetComponent.find('.widget__content__task:not(.done)'),
    ).toHaveLength(2);
    expect(WidgetComponent.find('.widget__content__task.done')).toHaveLength(2);
  });

  // Event tests
  it('chamar o evento de alterar status das tasks', () => {
    const handleChangeCheckbox = jest.fn(),
      props = {
        handleChangeCheckbox,
      };

    const WidgetComponent = mount(
      <WidgetView
        {...defaultProps}
        {...props}
        openTasks={[
          { label: 'Open Task 0', id: 0 },
          { label: 'Open Task 1', id: 1 },
        ]}
        doneTasks={[
          { label: 'Open Task 2', id: 2 },
          { label: 'Open Task 3', id: 3 },
        ]}
      />,
    );

    WidgetComponent.find('.widget__content__task:not(.done) input')
      .at(0)
      .simulate('change', { target: { checked: true } }, 0);

    expect(handleChangeCheckbox).toHaveBeenCalled();

    WidgetComponent.find('.widget__content__task.done input')
      .at(0)
      .simulate('change', { target: { checked: false } }, 2);

    expect(handleChangeCheckbox).toHaveBeenCalled();
  });

  //   Controller tests
  it('renderizar suas funções corretamente', () => {
    const WidgetComponent = mount(
      <WidgetController
        {...defaultProps}
        openTasks={[
          { label: 'Open Task 0', id: 0 },
          { label: 'Open Task 1', id: 1 },
        ]}
        doneTasks={[
          { label: 'Open Task 2', id: 2 },
          { label: 'Open Task 3', id: 3 },
        ]}
      />,
    );

    WidgetComponent.find('.widget__content__task:not(.done) input')
      .at(0)
      .simulate('change', { target: { checked: true } }, 0);

    expect(defaultProps.changeTaskStatus).toHaveBeenCalledWith({
      id: 0,
      status: 'COMPLETE_TASK',
    });

    WidgetComponent.find('.widget__content__task.done input')
      .at(0)
      .simulate('change', { target: { checked: false } }, 2);

    expect(defaultProps.changeTaskStatus).toHaveBeenCalledWith({
      id: 2,
      status: 'UNCOMPLETE_TASK',
    });
  });

  //   Reducer tests
  it('utilizar o reducer corretamente', () => {
    const initialState = {
      openTasks: [],
      doneTasks: [],
    };
    let action = {},
      state = {};

    //   Testando o GET_TASKS
    action = {
      type: constantsWidget.GET_TASKS,
      payload: { response: constantsWidget.responseMock },
    };

    state = reducersWidget(undefined, action);

    expect(state).toEqual({
      ...initialState,
      openTasks: [
        { label: 'Ir ao shopping', id: 0 },
        { label: 'Terminar o dever de casa', id: 1 },
        { label: 'Lavar louça', id: 2 },
        { label: 'Ir ao trabalho', id: 3 },
      ],
      doneTasks: [{ label: 'Fazer lanche', id: 4 }],
    });

    //   Testando o CHANGE_TASK_STATUS com COMPLETE_TASK
    action = {
      type: constantsWidget.CHANGE_TASK_STATUS,
      payload: { response: { id: 0, status: constantsWidget.COMPLETE_TASK } },
    };

    state = reducersWidget(state, action);

    expect(state).toEqual({
      openTasks: [
        { label: 'Terminar o dever de casa', id: 1 },
        { label: 'Lavar louça', id: 2 },
        { label: 'Ir ao trabalho', id: 3 },
      ],
      doneTasks: [
        { label: 'Fazer lanche', id: 4 },
        { label: 'Ir ao shopping', id: 0 },
      ],
    });

    //   Testando o CHANGE_TASK_STATUS com UNCOMPLETE_TASK
    action = {
      type: constantsWidget.CHANGE_TASK_STATUS,
      payload: { response: { id: 4, status: constantsWidget.UNCOMPLETE_TASK } },
    };

    state = reducersWidget(state, action);

    expect(state).toEqual({
      openTasks: [
        { label: 'Terminar o dever de casa', id: 1 },
        { label: 'Lavar louça', id: 2 },
        { label: 'Ir ao trabalho', id: 3 },
        { label: 'Fazer lanche', id: 4 },
      ],
      doneTasks: [{ label: 'Ir ao shopping', id: 0 }],
    });
  });
});
