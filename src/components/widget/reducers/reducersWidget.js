import {
  GET_TASKS,
  CHANGE_TASK_STATUS,
  COMPLETE_TASK,
  UNCOMPLETE_TASK,
} from '../constants/constantsWidget';

const initialState = {
  openTasks: [],
  doneTasks: [],
};

export default function reducersWidget(state = initialState, action) {
  switch (action.type) {
    case GET_TASKS: {
      const { openTasks, doneTasks } = action.payload.response;

      return {
        ...state,
        openTasks,
        doneTasks,
      };
    }

    case CHANGE_TASK_STATUS: {
      const { id, status } = action.payload.response;

      if (status === COMPLETE_TASK) {
        return {
          ...state,
          openTasks: state.openTasks.filter(elem => elem.id !== id),
          doneTasks: state.doneTasks.concat(
            state.openTasks.filter(elem => elem.id === id),
          ),
        };
      } else if (status === UNCOMPLETE_TASK) {
        return {
          ...state,
          openTasks: state.openTasks.concat(
            state.doneTasks.filter(elem => elem.id === id),
          ),
          doneTasks: state.doneTasks.filter(elem => elem.id !== id),
        };
      }
    }

    default:
      return state;
  }
}
