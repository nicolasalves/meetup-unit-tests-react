import React from 'react';
import { SettingsOutlined, AddOutlined } from '@material-ui/icons';
import { FormGroup, FormControlLabel, Checkbox } from '@material-ui/core';

// CSS
import './Widget.css';

const WidgetView = ({ openTasks, doneTasks, handleChangeCheckbox }) => (
  <div className="widget">
    <div className="widget__menu">
      <div className="widget__menu__left">
        <span>A fazer: Inbox</span>
      </div>
      <div className="widget__menu__right">
        <SettingsOutlined />
      </div>
    </div>
    <div className="widget__content">
      <FormGroup className="widget__content__tasks">
        {openTasks.map(task => (
          <FormControlLabel
            control={
              <Checkbox
                value={`task-${task.id}`}
                onChange={evt => handleChangeCheckbox(evt, task.id)}
              />
            }
            label={task.label}
            className="widget__content__task"
            key={task.id}
          />
        ))}
      </FormGroup>
      <div className="widget__content__add-tasks">
        <AddOutlined classes={{ root: 'widget__content__add-tasks__icon' }} />
        <span>Adicionar a fazer</span>
      </div>
      <hr />
      <FormGroup className="widget__content__done-tasks">
        {doneTasks.map(task => (
          <FormControlLabel
            control={
              <Checkbox
                value={`task-${task.id}`}
                onChange={evt => handleChangeCheckbox(evt, task.id)}
              />
            }
            label={task.label}
            className="widget__content__task done"
            checked
            key={task.id}
          />
        ))}
      </FormGroup>
    </div>
  </div>
);

export default WidgetView;
