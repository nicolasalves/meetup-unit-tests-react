import {
  GET_TASKS,
  CHANGE_TASK_STATUS,
  responseMock,
} from '../constants/constantsWidget';

export const getTasks = () => {
  return { type: GET_TASKS, payload: { response: responseMock, status: 200 } };
};

export const changeTaskStatus = ({ id, status }) => {
  return {
    type: CHANGE_TASK_STATUS,
    payload: { response: { id, status }, status: 200 },
  };
};
